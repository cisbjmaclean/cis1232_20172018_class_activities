package bubblesortb;

import java.util.Arrays;

/**
 * 
 * @since Jan 29, 2018
 * @author bjmaclean
 */
public class CisArrays {
    public static void sort(int[] a) {
        boolean debugging = false;
        boolean swapped = false;
        int lastSwapIndex = 10;
        int countCompares = 0;
        int currentEndLocation = a.length - 1;

        if(debugging)System.out.println("Initial array -->" + Arrays.toString(a));

        /*
        The first time we will loop from 0 to the end.  For each iteration we will
        loop from 0 to one less.  
         */
        for (currentEndLocation = a.length - 1; currentEndLocation > 0; currentEndLocation--) {
            //reset the swapped flag.
            swapped = false;
            
            if (debugging) {
                System.out.println("Current end location = " + currentEndLocation);
            }
            //Loop through the array from 0 to current location
            for (int index = 0; index < currentEndLocation; index++) {

                //Add a debug statement to show what we are comparing
                if (debugging) {
                    System.out.print("Comparing " + a[index] + " with " + a[index + 1]);
                }

                countCompares++;
                if (a[index] > a[index + 1]) { //if right value is > left, then swap
                    if(debugging)System.out.println("last swap index="+(index+1));
                    lastSwapIndex = index+1;
                    swapped = true;
                    if (debugging) {
                        System.out.print(" SWAPPED");
                    }
                    int temp = a[index];
                    a[index] = a[index + 1];
                    a[index + 1] = temp;
                }
                if (debugging) {
                    System.out.println("");
                }
            }
            if(debugging)System.out.println("End array -->" + Arrays.toString(a));
            if(!swapped){
                break; //There were no swaps so break.
            }else{
                if(debugging)System.out.println("last swap="+lastSwapIndex+" current end="+currentEndLocation);
                
                if(lastSwapIndex < currentEndLocation){
                    currentEndLocation = lastSwapIndex;
                }
            }
            
        }
        if(debugging)System.out.println("Count of compares="+countCompares);

    }

}
