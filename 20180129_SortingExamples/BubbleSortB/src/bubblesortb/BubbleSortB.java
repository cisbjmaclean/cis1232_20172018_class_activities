package bubblesortb;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 *
 * @since Jan 29, 2018
 * @author bjmaclean
 */
public class BubbleSortB {

    public static void main(String[] args) {
        int[] a = {3, 0, 1, 8, 7, 2, 5, 4, 6, 9};
        //int[] a = {0,1,2,3,4,5,6,7,8,9};

        int[] theArray = buildArray(100000);

        //System.out.println("Before sort -->"+Arrays.toString(a));
        LocalDateTime startTime;
        startTime = LocalDateTime.now();

       CisArrays.sort(theArray);
        //Arrays.sort(theArray);
        
        LocalDateTime endTime = LocalDateTime.now();

        System.out.println("Time=" + Duration.between(endTime, startTime));
    }

    public static int[] buildArray(int size) {
        int[] temp = new int[size];
        for (int i = 0; i < temp.length; i++) {
            Random rand = new Random();
            temp[i] = rand.nextInt(10000) + 1;
        }
        return temp;
    }

}
