package class5testb;

import java.util.ArrayList;

/**
 * Employee Class
 * @since Feb 26, 2018
 * @author bjmaclean
 */
public class Employee {
    
    private String name;
    private ArrayList<String>skills = new ArrayList<String>();

    public Employee() {
        
        String newSkill="";
        boolean hasMoreSkills=true;
        this.name= Util.getInputString("What is the Employee name?");
        
        do {
            newSkill="";
            newSkill=Util.getInputString("Enter a Skill please :D\n"
                    + "Enter to quit");
            if(newSkill.isEmpty())
            {
                hasMoreSkills=false;
            }
            else
            {
                skills.add(newSkill);
            }
        } while (hasMoreSkills);
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<String> skills) {
        this.skills = skills;
    }
    
    public boolean checkSkills(String input){
        
        boolean foundSkill=false;
        
        for (String skill : skills) {
            if (skill.equalsIgnoreCase(input)) {
                foundSkill=true;  
                break;
            }
        }
        return foundSkill;
        
        
    }

    @Override
    public String toString() {  
 
        String output="Employee Name:";
        output+= name+"\nSkills:";
        for (String skill : skills) {
            output+=skill+"\n";
        }
        return output;
    }
    
    
    
    

}
