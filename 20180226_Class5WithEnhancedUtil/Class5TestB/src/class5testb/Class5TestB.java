package class5testb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Class 5 Employee Program
 *
 * @since Feb 26, 2018
 * @author bjmaclean
 */
public class Class5TestB {

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add Employee A\n"
            + "- B-Show all Employees B\n"
            + "- C-Show Employees by skills C\n"
            + "- D-Sort Names \n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";


    private static HashMap<String, Employee> nameEmployeeMap= new HashMap<>();
    public static void main(String[] args) {

        String option;
        Util.setGUI(true);
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
//Add a switch to process the option
        switch (option) {
            case "A":
                Employee newEmployee = new Employee();
                nameEmployeeMap.
                nameEmployeeMap.put(newEmployee.getName(), newEmployee);
                break;
            case "B":
                showAllEmployees();
                break;
            case "C":
                showSkillEmployees();
                break;
            case "D":
                    sortPlease();
                    break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        //System.out.print(MENU);
        String option = Util.getInputString(menu);
        option = option.toUpperCase();
        return option;
    }
    
    public static void showAllEmployees()
    {
        nameEmployeeMap.values().forEach((object) -> {
            Util.display(object.toString());
        });
    }
     public static void showSkillEmployees()
    {
        String whatSkill = Util.getInputString("What Skill are you looking for today?");
        
        for (Employee value : nameEmployeeMap.values()) {
            if(value.checkSkills(whatSkill))
            {
               Util.display(value.toString()); 
            }
        }
        
        
    }
     
     public static void sortPlease()
     {
         ArrayList<String> sortList = new ArrayList<>(nameEmployeeMap.keySet());
         Collections.sort(sortList);//Sorting 
         for (String string : sortList) {
             Util.display(string);
         }
     }

}
