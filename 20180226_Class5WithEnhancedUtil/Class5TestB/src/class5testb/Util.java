package class5testb;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Utility class for generic program functions.
 *
 * @author BJ MacLean
 * @since Feb 27, 2015
 */
public class Util {
    public static final boolean DEBUGGING = false;
    private static Scanner input = new Scanner(System.in);
    private static boolean isGUI = false;
    
    /**
     * Get a scanner object.
     * @return Scanner
     * @since 20180226
     * @author CIS1232
     */
    
    public static Scanner getInput(){
        return input;
    }

    /**
     * Get input from the user using the appropriate input method
     * @return Scanner
     * @since 20180226
     * @author CIS1232
     */
    public static String getInputString(String output){
        if(isGUI) //use JoptionPane
        {
            return JOptionPane.showInputDialog(output);
        }
        else
        {
            display(output);
            return input.nextLine(); 
        }       
    }
    
    /**
     * Display output to the user using the appropriate input method
     * @return void
     * @since 20180226
     * @author CIS1232
     */
    
    public static void display(String output)
    {
        if(isGUI)
        {
            JOptionPane.showMessageDialog(null, output);
        }
        else
        {
        System.out.println(output);
        }
    }
    /**
     * Set the gui indicator.  If gui then JOptionPane will be used.
     * @author Brandon the great
     * @param setter 
     */
    public static void setGUI(boolean setter)
    {
        isGUI=setter;
    }
    
    
}
