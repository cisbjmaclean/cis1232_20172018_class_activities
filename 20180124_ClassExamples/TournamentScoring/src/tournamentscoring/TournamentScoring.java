package tournamentscoring;

import java.util.Scanner;

/**
 * This class will be the main class for implementing java array practicing. The
 * problems will be listed and described in this main class but will ask you to
 * create specific classes which contain attributes and methods to accomplish
 * several array processing tasks.
 *
 * @author BJ
 * @since 20140120
 */
public class TournamentScoring {

    public static final int NUMBER_PLAYERS = 20;
    private static int[] playerGoals1 = new int[NUMBER_PLAYERS];
    private static int[] playerGoals2 = new int[NUMBER_PLAYERS];
    private static int[] playerGoals3 = new int[NUMBER_PLAYERS];

//***********************************************************************
//Problem description
//You have been asked to create a program for a minor hockey team. The manager wants
//a program which will keep track to their players goals. The players are
//numbered from 1-20 and they will be playing 3 games during the tournament to be tracked.
//The program can remain running throughout the year.
//
//The program should allow the user to enter the game number, a player number
//and number of goals (when menu option A is chosen).
//
//At the end of the data entry or when the user selects S the program should invoke a method
//which will display the player numbers and their total number of goals.
//
//There will be three approaches that will be used:
//1) Use three one dimensional arrays (game1, game2, game3) which will hold the results
// for each game. Each element in the array represents the player number at that
// index.
//
//2) Use a two dimensional array to hold all of the results (goalDetails). The rows (first index)
// will represent each game and each row will contain the goals for the player number at
// the given index.
//
//3) Use a one dimensional array of a class. The Player class should have a playerNumber
// attribute and goals attribute which is an int[3] element where each int represents the goals
// that the player scored in each game.
//***********************************************************************
    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a player A\n"
            + "- S-Show players goals\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        String option;
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
//Add a switch to process the option
        switch (option) {
            case "A":
                System.out.print("Enter player number-->");
                int playerNumber = input.nextInt();
                input.nextLine(); //burn the line

                System.out.print("Enter game number-->");
                int gameNumber = input.nextInt();
                input.nextLine(); //burn the line

                System.out.print("Enter number of goals in that game-->");
                int numberOfGoals = input.nextInt();
                input.nextLine(); //burn the line

                if(gameNumber == 1){
                    playerGoals1[playerNumber-1] = numberOfGoals;
                }else if(gameNumber == 2){
                    playerGoals2[playerNumber-1] = numberOfGoals;
                }else if(gameNumber == 3){
                    playerGoals3[playerNumber-1] = numberOfGoals;
                }else{
                    System.out.println("invalid game number");
                }
                
                break;
            case "S":
                showAllPlayerGoals();
                break;
//            case "C":
//                System.out.println("User picked c");
//                break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    public static void showAllPlayerGoals() {
        System.out.println("Player#\tGoals\t");
                for (int playerNumber = 0; playerNumber < NUMBER_PLAYERS; playerNumber++) {
                    
                    //have to add up this players goals
                    int goals = playerGoals1[playerNumber]
                            +playerGoals2[playerNumber]
                            +playerGoals3[playerNumber];
                    
                    
                    System.out.println((playerNumber+1)+"\t"+goals );
                }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        System.out.print(MENU);
        String option = input.nextLine();
        option = option.toUpperCase();
        return option;
    }

}
