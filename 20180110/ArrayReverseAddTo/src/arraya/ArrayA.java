package arraya;

import java.util.Arrays;

/**
 * Some testing with arrays
 * @since Jan 8, 2018
 * @author cis1232
 */
public class ArrayA {

    public static void main(String[] args) {

        double[] theDoubles = {1.0, 2.0, 3.0, 4.0};
        
        //pass the array to a reverse method
        System.out.println(Arrays.toString(theDoubles));


        //this is one aproach to reversing
        //reverse(theDoubles);

        //this is a second approach.
        reverseWithoutDuplicateArray(theDoubles);
        System.out.println(Arrays.toString(theDoubles));  
        
        
        //*******************************************************
        // Add to array
        //********************************************************
        
        
        int[] theInts = {1,2,3};
        System.out.println("TESTING addToArray");
        
        theInts = addToArray(theInts, 4);
        
        for(int current: theInts){
            System.out.println(current);
        }
    }

    /**
     * This method will add a number to an array.  It will have to 
     * create a new array which is one longer than the original, copy
     * each element from the original to the first of the array and then
     * add the int passed in as the last element of the array.
     * @param original the original array
     * @param numberToAdd number to add to the output array.
     * @return the new array which is one longer than the original.
     */
    public static int[] addToArray(int[] original, int numberToAdd){
        
        //Create the array which will be one longer 
        int[] outArray = new int[original.length+1];
        
        //Copy the elements of the original to the first part of the new array
        for(int counter = 0; counter < original.length; ++counter){
            outArray[counter] = original[counter];
        }
        
        //Set the last element to the int passed in.
        outArray[original.length] = numberToAdd;
        
        //return the array
        return outArray;
    }
    
    /**
     * This method will reverse any double array which is passed in.  Note that 
     * arrays are pass by reference.  This means there is only one array in 
     * memory.  The in parameter is a reference to the array.
     * @param arrayToProcess Array to reverse.
     * @since 20180110
     * @author CIS1232
     */
    public static void reverse(double[] arrayToProcess){
        
        //Create a temporary array which is to be modified.
        double[] temp = new double[arrayToProcess.length];
        int counterUp = 0;
        
        //**********************************************************
        //Loop through the array from end to start and assign to 
        //the temporary array
        //**********************************************************
        for(int counter = temp.length-1; counter >= 0; --counter){
            temp[counterUp] = arrayToProcess[counter];
            counterUp++;
        }
        
        //***********************************************************
        //Since we are not returning an array, we will modify the values
        //of the original array.  This will result in the array which is referenced
        //being updated.
        //***********************************************************
        
        for(int counter = 0; counter < temp.length; ++counter){
            arrayToProcess[counter]= temp[counter];
        }
        
    }
    
    /**
     * Another approach simpler but somehow more complex.  
     * @param arrayToProcess 
     */
    
    public static void reverseWithoutDuplicateArray(double[] arrayToProcess){
        
        double temp1;
        int length = arrayToProcess.length;
        for(int counter = 0; counter < length/2; counter++){
            
            temp1 = arrayToProcess[counter];
            arrayToProcess[counter] = arrayToProcess[length-counter-1];
            arrayToProcess[length-counter-1] = temp1;
            
            
        }
    }
    
}
