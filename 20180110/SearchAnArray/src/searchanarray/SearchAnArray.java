package searchanarray;

/**
 * An alternative search approach (from Joyce Farrell's)
 * @since Jan 10, 2018
 * @author bjmaclean
 */
public class SearchAnArray {

    public static void main(String[] args) {

        int[] ids = {5,8,12,20,21};

        int idToLookFor = 12;
        
        boolean found = false;
        //traditional for loop
        for(int counter = 0; counter < ids.length; counter++){
            if(ids[counter] == idToLookFor){
                found = true;
                break;
            }
        }

        //enhanced for loop
//        for(int currentElement : ids){
//            if(currentElement == idToLookFor){
//                found = true;
//                break;
//            }
//        }
        
        if(found){
            System.out.println("valid");
        }else{
            System.out.println("invalid");
        }
            
        
    }

}
