package arraymulticlassseatingexample;

/**
 * A student has a name
 *
 * @since Jan 22, 2018
 * @author bjmaclean
 */
public class Student {

    private String name;
    public static final String EMPTY = "-";

    public Student(){
        this.name = EMPTY;
    }
        public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public void display() {
        System.out.println(toString());
    }
}
