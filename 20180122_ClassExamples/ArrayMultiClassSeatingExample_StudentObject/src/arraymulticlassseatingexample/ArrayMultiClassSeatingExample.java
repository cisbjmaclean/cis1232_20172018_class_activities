package arraymulticlassseatingexample;

/**
 * This will use a 2d array to hold class seating information
 *
 * @since Jan 22, 2018
 * @author bjmaclean
 */
public class ArrayMultiClassSeatingExample {

    public static final int NUMBER_OF_ROWS = 3;
    public static final int NUMBER_OF_COLUMNS = 6;

    public static void main(String[] args) {

        //Create the 2d array structure

        Student seats[][] = new Student[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
        
        //***********************************************************************
        //Loop through seats.  If the element is not null and not equal to EMPTY
        //then set it to EMPTY;
        //***********************************************************************
        for (int rowIndex = 0; rowIndex < seats.length; rowIndex++) {
            int numberOfSeatsInRow = seats[rowIndex].length;
            for (int seatIndex = 0; seatIndex < numberOfSeatsInRow; seatIndex++) {
                if (seats[rowIndex][seatIndex] == null ) {
                    seats[rowIndex][seatIndex] = new Student();
                }
            }
        }

        seats[0][0].setName("Noel");
        seats[1][0].setName("Laura");
        seats[1][2].setName("Sinem");
        seats[1][5].setName("Darren");
        seats[2][0].setName("Mackenzie");
        seats[2][2].setName("Nic");
        seats[2][3].setName("Chris");
        seats[2][4].setName("Ashleigh");

        //display the seating using a for loop
        displayClassUsingForLoop(seats);
    }

    /**
     * Display the seats using nested for loops
     *
     * @param seats
     * @since 20180122
     * @author BJM/CIS1232A
     */
    public static void displayClassUsingForLoop(Student[][] seats) {

        System.out.println("BJM Debug:  length of seats=" + seats.length);
        System.out.println("BJM Debug: length of first row in seats=" + seats[0].length);

        int numberOfRows = seats.length;

        for (int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            int numberOfSeatsInRow = seats[rowIndex].length;
            for (int seatIndex = 0; seatIndex < numberOfSeatsInRow; seatIndex++) {
                //*********************************************************************
                //If the name that is being output is less than 8 we will add two tabs.
                //If the name is greater than or equal to 8 we will only add one tab. This 
                //will help line things up in the console.
                //*********************************************************************
                String tabs = "\t";
                if (seats[rowIndex][seatIndex].getName().length() < 8) {
                    tabs += "\t";
                }
                System.out.print(seats[rowIndex][seatIndex].toString() + tabs);
            }
            //*********************************************************************
            //After each row is processed, add a line feed so the next row will
            //start on a new line.
            //*********************************************************************
            System.out.println("");
        }
    }

}
