package gradesparallela;

/**
 * This class will contain attributes and methods associated 
 * with processing class grades.
 * @since Jan 15, 2018
 * @author bjmaclean
 */
public class CourseSection {

    public static final int NOT_FOUND = -1;
    private String sectionName;
    private String[] names = {"Darren","Darcy","Nic","Wayne"};
    private int[] assignmentGrades = {90,88,75,40};
    private int[] theoryGrades = {98,60,80,90};
    private int[] practicalGrades = {32,85,80,90};
    
    public CourseSection(String name){
        this.sectionName = name;
    }
    
    public void display(){
        System.out.println(this.toString());
    }
    /**
     * This method will take a name and determine the average for
     * the given student.  It will return -1 if name not found.
     * @param name student name
     * @return the average or -1.0 if not found
     * @since 20180115
     * @author cis1232
     */
    public double getAverageByName(String name){
        //Find index of name
        double average = NOT_FOUND;
        for (int count = 0; count < names.length; count++) {
            if(name.equalsIgnoreCase(names[count])){
                average = getAverage(count);
                break;
            };       
        }
        return average;
    }
    
    /**
     * This method will get the average for the student
     * at the given index.
     * @param index the student index in the parallel arrays
     * @return their average
     * @since 20180115
     * @author CIS1232A
     */
    public double getAverage(int index){
        return (assignmentGrades[index]
                    +theoryGrades[index]
                    +practicalGrades[index])/3.0;
    }
    
    public String toString(){
        
        String output = "Section name: "+sectionName+System.lineSeparator();
        output += "Name\tassignment\ttheory\tpractical\taverage"+System.lineSeparator();
        
        //Use the arrays to show the section information
        for(int counter = 0; counter < names.length; ++counter){
            double average = getAverage(counter);
            output += names[counter]+"\t"
                    +assignmentGrades[counter]+"\t\t"
                    +theoryGrades[counter]+"\t"
                    +practicalGrades[counter]+"\t\t"
                    +average
                    +System.lineSeparator();
        }
        
        return output;
    }
}
