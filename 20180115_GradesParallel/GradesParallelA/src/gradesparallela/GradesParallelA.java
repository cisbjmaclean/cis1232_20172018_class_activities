package gradesparallela;

import java.util.Scanner;

/**
 * This program will track student grades using parallel arrays.
 *
 * @since Jan 15, 2018
 * @author bjmaclean
 */
public class GradesParallelA {

    private static CourseSection courseSection = new CourseSection("CIS1232A");
    private static Scanner input = new Scanner(System.in);

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Process A\n"
            + "- B-Get average for student by name\n"
            + "- S-Show section\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        String option;
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option) {
            case "A":
                System.out.println("User picked a");
                break;
            case "B":
                System.out.print("Enter name-->");

                String name = input.nextLine();
                double average = courseSection.getAverageByName(name);
                if (average > CourseSection.NOT_FOUND) {
                    System.out.println("Average=" + average + "%");
                } else {
                    System.out.println("Student not found");
                }
                break;
            case "S":
                courseSection.display();
                break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        System.out.print(MENU);
        String option = input.nextLine();
        option = option.toUpperCase();
        return option;
    }

}
