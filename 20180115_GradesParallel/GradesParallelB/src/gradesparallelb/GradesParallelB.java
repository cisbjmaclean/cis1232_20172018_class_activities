package gradesparallelb;

import java.util.Scanner;

/**
 *
 * @since Jan 15, 2018
 * @author bjmaclean
 */
public class GradesParallelB {

    private static CourseSection courseSection = new CourseSection("CIS1232B");
    private static Scanner input = new Scanner(System.in);

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Set new grades for a student\n"
            + "- B-Get average for student by name\n"
            + "- S-Show section information\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        String option;
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
//Add a switch to process the option
        switch (option) {
            case "A":
                System.out.println("Enter name:");
                String name2 = input.nextLine();
                courseSection.setGrades(name2);
                break;
            case "B":
                System.out.println("Enter name:");
                String name = input.nextLine();
                int index = courseSection.indexOf(name);
                if(index == CourseSection.NOT_FOUND){
                    System.out.println("Not found");
                }else{
                    System.out.println("Average="+courseSection.getAverage(index));
                }
                break;
            case "S":
                courseSection.display();
                break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        System.out.print(MENU);
        String option = input.nextLine();
        option = option.toUpperCase();
        return option;
    }

}
