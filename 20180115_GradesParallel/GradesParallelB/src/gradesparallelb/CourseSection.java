package gradesparallelb;

/**
 * This class will contain attributes and methods associated with a course
 * section.
 *
 * @since Jan 15, 2018
 * @author cis1232b
 */
public class CourseSection {

    public static final int NOT_FOUND = -1;
    private String name = "";
    private String[] names = {"Pedra", "Jennifer", "Darcy", "Brandon"};
    private int[] assignmentGrades = {90, 88, 75, 40};
    private int[] theoryGrades = {98, 60, 80, 90};
    private int[] practicalGrades = {32, 85, 80, 90};

    public CourseSection(String name) {
        this.name = name;
    }

    /**
     * This method will return the index for a given name
     *
     * @param name Name to find
     * @return index corresponding to name, -1 if not found
     * @author CIS1232B
     * @since 20180115
     */
    public int indexOf(String name) {

        int foundAtLocation = NOT_FOUND;

        //Loop through the names and return the index where the name is found.
        for (int index = 0; index < names.length; index++) {
            if (name.equalsIgnoreCase(names[index])) {
                foundAtLocation = index;
                break;
            }

        }
        return foundAtLocation;
    }

/**
 * Set the value of a student grades by name.  This method will
 * find the index for the name and then prompt the user for new
 * values for their pratical, assignment, and theory grades.
 * @param name 
 * @since 20180115
 * @author CIS1232B
 */
    
    public void setGrades(String name){
        //Get the index (use indexOf method)
        
        //Prompt and set each component's grade
    }
    
    /**
     * Method to get the average of the student at the specified index
     * of the parallel arrays.
     * @param index The subscript indicating which student
     * @return the average 
     * @author CIS1232B
     * @since 20180115
     */
    
    public double getAverage(int index) {
        int practical = practicalGrades[index];
        int theory = theoryGrades[index];
        int assignment = assignmentGrades[index];
        double average = (practical + theory + assignment) / 3.0;
        return average;
    }

    public void display() {
        System.out.println(toString());
    }

    public String toString() {
        String output = "Section: " + name + System.lineSeparator();
        output += "Name\tAssignment\tTheory\tPractical\tAverage" + System.lineSeparator();

        for (int count = 0; count < names.length; count++) {
            String name = names[count];
            int practical = practicalGrades[count];
            int theory = theoryGrades[count];
            int assignment = assignmentGrades[count];
            double average = getAverage(count);
            String tabs = "\t";

            //If the name is short add a tab for formatting output.
            if (name.length() < 8) {
                tabs += "\t";
            }
            output += name + tabs + assignment + "\t" + theory + "\t" + practical + "\t\t" + average + System.lineSeparator();
        }
        return output;
    }
}
