/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cis1232_lab_20170111;

/**
 *
 * @author bjmaclean
 */
public class ArrayProcessing {

    /**
     * ACTIVITY 1 This method should return the average of all of the number
     * provided in the inArray. The average should then be passed back from the
     * method.
     *
     * @since 20170111
     * @author CIS
     */
    public static double getAverage(int[] inArray) {
        int sum = 0;
        for(int current: inArray){
            sum += current;
        }
        return sum/inArray.length;
    }

    /**
     * ACTIVITY 2 This method should return the total of all of the number
     * provided in the inArray. The average should then be passed back from the
     * method.
     *
     * @since 20170111
     * @author CIS
     */
    public static int getTotal(int[] inArray){
        return 0;
    }

    /**
     * ACTIVITY 3
     * This method should return the number of elements from the inArray that are even numbers.
     *
     * @since 20170111
     * @author CIS
     */
    public static int getNumberOfEvenNumbers(int[] inArray){
        return 0;
    }

    /**
     * ACTIVITY 4
     * This method should return the number of elements from the inArray that multiples
     * of the divisor that is passed in as a parameter.
     *
     * @since 20170111
     * @author CIS
     */
    public static int getNumberDivisible(int[] inArray, int divisor){
        return 0;
    }

    /**
     * ACTIVITY 5
     * This method should return the most popular int.  It can be assumed that the elements
     * in the inArray are between 1-10.  If there is more than one that is the most popular then 
     * the method should just return the first one.  Ie {1,3,3,4,4} would return 3.
     *
     * @since 20170111
     * @author CIS
     */
    public static int getMostPopular(int[] inArray){
        return 0;
    }
    
    /**
     * ACTIVITY 6
     * This method should return true of false based on whether the inArray contains
     * any duplicates.  If there are any duplicates then return true otherwise return false.
     *
     * @since 20170111
     * @author CIS
     */
    public static boolean getIfHasDuplicates(int[] inArray){
        return false;
    }
    
}
