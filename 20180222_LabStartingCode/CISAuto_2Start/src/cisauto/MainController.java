package cisauto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Main class to control overall program functionality.
 *
 * @author BJ MacLean
 * @since Nov 5, 2014
 */
public class MainController {

    /**
     * @param args the command line arguments
     */
    private static HashMap<Integer, Vehicle> inventory = new HashMap();
    private static ArrayList<Sale> sales = new ArrayList();

    public static final String NAME_OF_BUSINESS = "CIS Auto Sales";

    public static final String MENU = NAME_OF_BUSINESS + "\n"
            + "Choose one  of the following options:\n"
            + "A - Add a vehicle\n"
            + "B - Sell a vehicle\n"
            + "C - Show inventory\n"
            + "D - Display number of vehicles in inventory\n"
            + "E - Get report of day sales\n"
            + "F = Report- Summary for given salesperson\n"
            + "G = Report- Summary for Make of Vehicle\n"
            + "X – eXit";

    public static final String SALES_REPORT_HEADING = "SALES REPORT";

    public static Scanner input = new Scanner(System.in);

    public static final String INVENTORY_COUNT_OUTPUT_1 = NAME_OF_BUSINESS + "";
    public static final String INVENTORY_COUNT_OUTPUT_2 = "Number of cars:  #";
    public static final String INVENTORY_COUNT_OUTPUT_3 = "Number of trucks:  #";
    public static final String INVENTORY_COUNT_OUTPUT_4 = "Number of other:  #";

    public static void main(String[] args) {

        Vehicle testOne = new Vehicle(Vehicle.TYPE_CAR, 6000.0, 8000, "Honda", "Accord", 2009, Vehicle.StatusType.INSTOCK);
        Vehicle testTwo = new Vehicle(Vehicle.TYPE_CAR, 3000.0, 5000, "Ford", "Focus", 2007, Vehicle.StatusType.INSTOCK);

        inventory.put(testOne.getVehicleId(), testOne);
        inventory.put(testTwo.getVehicleId(), testTwo);

        String userOption = null;

        do {
            userOption = processOption();
        } while (!userOption.equalsIgnoreCase("Q"));

    }

    /**
     * Processed the main menu options.
     * @author cis1232
     * @since 20180215
     * @return menu option chosen
     */
    
    public static String processOption() {
        System.out.println(MENU);
        String userOption = input.nextLine().toUpperCase();

        if (Util.DEBUGGING) {
            System.out.println("Processing for " + userOption);
        }
        switch (userOption) {
            case "A":
                Vehicle newVehicle = new Vehicle();
                newVehicle.getInformation();
                inventory.put(newVehicle.getVehicleId(), newVehicle);
                if (Util.DEBUGGING) {
                    System.out.println("Added:" + newVehicle);
                }
                break;
            case "B":
                sell();
                break;
            case "C":
                showInventory();
                break;
            case "D":
                showInventoryCounts();
                break;
            case "E":
                getDaySales();
                break;
            case "X":
                System.out.println("Goodbye");
                break;
            default:
                System.out.println("Invalid option");
        }
        return userOption;
    }

    /**
     * This will provide a report of sales for a given day.
     *
     * @since 20150303
     * @author cis1232
     */
    public static void getDaySales() {

        //Get the day for the report
        System.out.println("Enter date for sales(yyyymmdd):");
        String dateOfReport = Util.getInput().nextLine();
        if (Util.DEBUGGING) {
            System.out.println("about to run report for:" + dateOfReport);
        }

        //Header information for report
        System.out.println("");
        System.out.println(SALES_REPORT_HEADING + ":  " + dateOfReport);

        double totalSales=0;
        boolean foundASale = false;
        
        //for each sale
        for (Sale currentSale : sales) {
            String output = "";
            //If the date of sale if the desired report date, then proceed with it
            //in the report.
            if (currentSale.getDateOfSale().equalsIgnoreCase(dateOfReport)) {
                foundASale = true;
                totalSales += currentSale.getSoldPrice();
                
                if (Util.DEBUGGING) {
                    System.out.println("Found a sale:" + currentSale.toString());
                }

                //Get the vehicle details for the vehicle id that is specified in 
                //the sale.
                Vehicle vehicleSold = inventory.get(currentSale.getVehicleId());

                //Show the details for this sale
                output = "Name:  " + currentSale.getSalesPersonName() + "\n"
                        + "Sold price:  $" + currentSale.getSoldPrice() + "\n"
                        + "Vehicle ID:  " + currentSale.getVehicleId() + "\n"
                        + "Make:  " + vehicleSold.getMake() + "\n"
                        + "Model:  " + vehicleSold.getModel() + "\n"
                        + "List Price:  $" + vehicleSold.getListPrice() + "\n\n";
                System.out.println(output);

            }
        }
        if(foundASale){
            System.out.println("\nTotal sales:  $"+totalSales+"\n\n");
        } else {
            System.out.println("\nNo sales for this day.\n\n");
        }
        

    }

    /**
     * Sell a vehicle
     *
     * @author CIS1232
     * @since 20150303
     */
    public static void sell() {
        //System.out.println("Future functionality");
        System.out.println("Selling a vehicle:");
        System.out.println("Enter the vehicle ID");
        int vehicleId = Integer.parseInt(Util.getInput().nextLine());


        //Check to make sure the vehicle is in stock
        
        if ((!inventory.containsKey(vehicleId))
                || ((inventory.get(vehicleId).getStatusType() != Vehicle.StatusType.INSTOCK))) {
            System.out.println("Vehicle is not for sale");
        } else {
            //Get the remaining information
            System.out.println("Sales person name:");
            String salesPersonName = Util.getInput().nextLine();
            System.out.println("Sold price:  ");
            double soldPrice = Double.parseDouble(Util.getInput().nextLine());
            System.out.println("Date of sale (yyyymmdd):");
            String dateOfSale = Util.getInput().nextLine();

            //Add the new sale to the collection of sales
            Sale newSale = new Sale(dateOfSale, soldPrice, vehicleId, salesPersonName);
            sales.add(newSale);
            inventory.get(vehicleId).setStatusType(Vehicle.StatusType.SOLD);
        }
    }

    public static void showInventory() {
        //System.out.println("Future functionality");
        System.out.println("Inventory:");
        for (Integer i : inventory.keySet()) {
            System.out.println(inventory.get(i));
        }
    }

    /**
     * This will show the inventory counts based on vehicle type
     *
     * @since 20150227
     * @author cis1232
     */
    public static void showInventoryCounts() {
        int countCars = 0;
        int countTrucks = 0;
        int countOther = 0;

        //Count the vehicles based on status and type.
        for (Integer i : inventory.keySet()) {
            //if they are instock then count them
            Vehicle vehicle = inventory.get(i);
            if (vehicle.getStatusType() == Vehicle.StatusType.INSTOCK) {
                switch (vehicle.getVehicleTypeCode()) {
                    case Vehicle.TYPE_CAR:
                        countCars++;
                        break;
                    case Vehicle.TYPE_TRUCK:
                        countTrucks++;
                        break;
                    case Vehicle.TYPE_OTHER:
                        countOther++;
                        break;

                }
            }
        }

        //Show the information
        String output = INVENTORY_COUNT_OUTPUT_1 + System.lineSeparator()
                + INVENTORY_COUNT_OUTPUT_2 + countCars + System.lineSeparator()
                + INVENTORY_COUNT_OUTPUT_3 + countTrucks + System.lineSeparator()
                + INVENTORY_COUNT_OUTPUT_4 + countOther + System.lineSeparator();

        System.out.println(output);

    }
}
