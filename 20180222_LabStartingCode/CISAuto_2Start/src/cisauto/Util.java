package cisauto;

import java.util.Scanner;

/**
 * Utility class for generic program functions.
 *
 * @author BJ MacLean
 * @since Feb 27, 2015
 */
public class Util {
    public static final boolean DEBUGGING = false;
    
    private static Scanner input = new Scanner(System.in);
    
    public static Scanner getInput(){
        return input;
    }
    
    
}
