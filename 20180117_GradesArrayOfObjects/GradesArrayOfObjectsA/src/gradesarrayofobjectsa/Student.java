package gradesarrayofobjectsa;

import java.util.Scanner;

/**
 * This class will represent a student.
 *
 * @since Jan 17, 2018
 * @author bjmaclean
 */
public class Student {

    public static final int NUMBER_OF_GRADES = 3;

    public static final int ASSIGNMENT = 0;
    public static final int THEORY = 1;
    public static final int PRACTICAL = 2;

    private String name;
    private int id; //1-20
    private int[] grades = {-1, -1, -1};
    private static Scanner input = new Scanner(System.in);

    public Student() {

        System.out.println("Enter name");
        name = input.nextLine();
        System.out.println("Enter id");
        id = input.nextInt();
        input.nextLine(); //burn the line
    }

    public void getGrade() {
        System.out.println("What grade do you want to enter(0=Asst 1=Theory 2=Practical)");
        int indexToUpdate = input.nextInt();
        input.nextLine(); //burn the line
        System.out.println("What was the grade?");
        int grade = input.nextInt();
        input.nextLine(); //burn the line
        grades[indexToUpdate] = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * This method will give back the average of grades entered so far in the
     * course.
     *
     * @return The average
     * @since 20180117
     * @author cis1232a
     */
    public double getAverage() {
        int total = 0;
        int count = 0;
        for (int current : grades) {
            if (current >= 0) {
                count++;
                total += current;
            }
        }
        if (count > 0) {
            return total / (double) count;
        } else {
            return 0;
        }

    }

    /**
     * This method will provide the average. If flag ofGradesSoFar is true, it
     * will return the average of grades entered so far, if not it will return
     * the average of grades including 0s for grades that were not yet entered.
     *
     * @param ofGradesSoFar indicates to only account for grades entered
     * @return average
     * @author cis1232a
     * @since 20180117
     */
    public double getAverage(boolean ofGradesSoFar) {
        double average;
        if (ofGradesSoFar) {
            average = getAverage();
        } else {
            int total = 0;
            int count = 0;
            for (int current : grades) {
                if (current >= 0) {
                    count++;
                    total += current;
                }
            }
            average = total / (double) NUMBER_OF_GRADES;

        }
        return average;
    }

    public int countGradesEntered() {
        int count = 0;
        for (int current : grades) {
            if (current >= 0) {
                count++;
            }
        }
        return count;
    }

    public void display() {
        System.out.println(toString());
    }

    public String toString() {
        String output = "Name: " + name + System.lineSeparator()
                + "ID: " + id + System.lineSeparator();
        //If assignment was entered
        if (grades[ASSIGNMENT] >= 0) {
            output += "Assignment: " + grades[ASSIGNMENT] + "%\n";
        }
        if (grades[THEORY] >= 0) {
            output += "Theory: " + grades[THEORY] + "%\n";
        }
        if (grades[PRACTICAL] >= 0) {
            output += "Practical: " + grades[PRACTICAL] + "%\n";
        }
        output += "Average: " + getAverage() + "\n";
        output += "Average overall out of 100: " + getAverage(false) + System.lineSeparator();
        return output;
    }
}
