package gradesarrayofobjectsa;

import java.util.Scanner;

/**
 *
 * @since Jan 17, 2018
 * @author bjmaclean
 */
public class GradesArrayOfObjectsA {

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a Student\n"
            + "- B-Show students\n"
            + "- C-Add grade\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static final int NUMBER_OF_STUDENTS = 20;
    private static Student[] students = new Student[NUMBER_OF_STUDENTS];

    public static void main(String[] args) {

        String option;
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option) {
            case "A":
                Student newStudent = new Student();
                students[newStudent.getId() - 1] = newStudent;
                break;
            case "B":
                for (Student current : students) {
                    if (current != null) {
                        current.display();
                    }
                }
                break;
            case "C":
                System.out.println("Enter student id");
                Scanner input = new Scanner(System.in);  //input object
                int id = input.nextInt();
                input.nextLine(); //burn the line
                students[id-1].getGrade();
                break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        System.out.print(MENU);
        Scanner input = new Scanner(System.in);
        String option = input.nextLine();
        option = option.toUpperCase();
        return option;
    }

}
