package gradesarrayofobjectsb;

import java.util.Scanner;

/**
 * This will represent a student.
 *
 * @since Jan 17, 2018
 * @author bjmaclean
 */
public class Student {

    private String name;
    private int id;
    public static final int NUMBER_OF_GRADES = 3;
    public static final int NOT_ENTERED = -1;

    private int[] grades = new int[NUMBER_OF_GRADES];
    private String[] gradeLabels = {"Assignment", "Theory", "Practical"};

    private static Scanner input = new Scanner(System.in);  //input object

    public Student() {
        System.out.println("Enter name");
        name = input.nextLine();
        System.out.println("Enter id");
        id = input.nextInt();
        input.nextLine(); //burn the line

        //initialize to -1
        for (int i = 0; i < grades.length; i++) {
            grades[i] = NOT_ENTERED;
        }

    }

    /**
     * Method will get the grade for a particular deliverable.
     *
     * @since 20180117
     * @author cis1232b
     */
    public void getGrade() {
        System.out.println("For what deliverable");
        for (int i = 0; i < gradeLabels.length; i++) {
            System.out.println(i + "=" + gradeLabels[i]);
        }
        System.out.print("-->");
        int index = input.nextInt();
        input.nextLine(); //burn the line
        System.out.println("Enter grade");
        int grade = input.nextInt();
        input.nextLine(); //burn the line

        //update the element
        grades[index] = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * This method will return the average of any grades that have been entered
     * so far
     *
     * @return average or -1 if no grades
     * @since 20180117
     * @author CIS1232B
     *
     */
    public double getAverage() {
        int total = 0;
        int count = 0;
        for (int currentValue : grades) {
            if (currentValue != NOT_ENTERED) {
                total += currentValue;
                count++;
            }
        }
        if (count > 0) {
            return (double) total / count;
        } else {
            return -1;
        }

    }

    public void display() {
        System.out.println(toString());
    }

    public String toString() {
        String output = "Name: " + name + System.lineSeparator()
                + "ID: " + id + System.lineSeparator();

        for (int index = 0; index > grades.length; index++) {
            if (grades[index] != NOT_ENTERED) {
                output += gradeLabels[index] + ": " + grades[index] + System.lineSeparator();
            }
        }

        double average = getAverage();
        if (average >= 0) {
            output += "Average: " + getAverage();
        }
        return output;
    }

}
