package gradesarrayofobjectsb;

import java.util.Scanner;

/**
 * This class will maintain grades for a class of up to 20 students.
 *
 * @since Jan 17, 2018
 * @author bjmaclean
 */
public class GradesArrayOfObjectsB {

    public static final int NUMBER_OF_STUDENTS = 20;
    private static Student[] students = new Student[NUMBER_OF_STUDENTS];

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add student\n"
            + "- B-Show students\n"
            + "- C-Add a grade\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        String option;
        do {
            option = getMenuOption(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase("X"));
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        switch (option) {
            case "A":
                Student student = new Student();
                students[student.getId() - 1] = student;
                break;
            case "B":
                for (Student current : students) {
                    if (current != null) {
                        current.display();
                    }

                }
                break;
            case "C":
                Scanner input = new Scanner(System.in);  //input object
                System.out.println("Enter id");
                int id = input.nextInt();
                input.nextLine(); //burn the line
                if (students[id - 1] != null) {
                    students[id - 1].getGrade();
                }else{
                    System.out.println("Student not found");
                }
                break;
            case "X":
                System.out.println("User picked x");
                break;
            default:
                System.out.println("Invalid entry");
        }
    }

    /**
     * This method will prompt the user based on the string passed in and return
     * their option.
     *
     * @param menu
     * @return
     */
    public static String getMenuOption(String menu) {
        System.out.print(MENU);
        Scanner input = new Scanner(System.in);
        String option = input.nextLine();
        option = option.toUpperCase();
        return option;
    }

}
